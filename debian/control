Source: libcomps
Section: libs
Priority: optional
Maintainer: RPM packaging team <team+pkg-rpm@tracker.debian.org>
Uploaders: Frédéric Pierret <frederic.pierret@qubes-os.org>,
           Luca Boccassi <bluca@debian.org>,
Build-Depends: debhelper-compat (= 13),
 dh-python,
 python3-dev,
 libpython3-dev,
 cmake,
 pkgconf,
 libxml2-dev,
 libexpat1-dev,
 doxygen,
 python3-sphinx,
 check,
 zlib1g-dev,
 python3-setuptools
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/rpm-software-management/libcomps
Vcs-Browser: https://salsa.debian.org/pkg-rpm-team/libcomps
Vcs-Git: https://salsa.debian.org/pkg-rpm-team/libcomps.git
#Testsuite: autopkgtest-pkg-python

Package: libcomps-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
 libcomps0 (= ${binary:Version}),
 libglib2.0-dev,
 libssl-dev,
 libcurl4-gnutls-dev,
 libattr1-dev,
 libgpgme-dev,
Description: Comps XML file manipulation library - development files
 Libcomps is library for structure-like manipulation of content in
 comps XML files. Supports reading/writing XML files and structure
 modifications.
 .
 This package contains the development header files for the
 libcomps library.

Package: libcomps0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
Provides: libcomps
Description: Comps XML file manipulation library - shared library
 Libcomps is library for structure-like manipulation of content in
 comps XML files. Supports reading/writing XML files and structure
 modifications.
 .
 This package provides the libcomps shared library.

Package: libcomps-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
 libjs-jquery,
Description: Documentation for the libcomps library (common documentation)
 Libcomps is library for structure-like manipulation of content in
 comps XML files. Supports reading/writing XML files and structure
 modifications.
 .
 This package installs common documentation for the libcomps
 C bindings.

Package: python-libcomps-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends},
 libjs-jquery,
 libjs-underscore,
Description: Python 3 bindings for the libcomps library (common documentation)
 Libcomps is library for structure-like manipulation of content in
 comps XML files. Supports reading/writing XML files and structure
 modifications.
 .
 This package installs common documentation for the libcomps Python 3
 bindings.

Package: python3-libcomps
Architecture: any
Multi-Arch: no
Section: python
Depends: ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends},
 libcomps0 (= ${binary:Version}),
Provides: ${python3:Provides},
Suggests: python-libcomps-doc
Description: Python bindings for the libcomps library (Python 3)
 Libcomps is library for structure-like manipulation of content in
 comps XML files. Supports reading/writing XML files and structure
 modifications.
 .
 This package installs the libcomps library for Python 3.
